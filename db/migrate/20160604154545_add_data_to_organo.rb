class AddDataToOrgano < ActiveRecord::Migration
  def change
    add_column :organos, :author, :string
    add_column :organos, :year, :integer
    add_column :organos, :price, :integer
  end
end
