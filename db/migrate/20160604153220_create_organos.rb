class CreateOrganos < ActiveRecord::Migration
  def change
    create_table :organos do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
