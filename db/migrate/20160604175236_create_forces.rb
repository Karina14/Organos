class CreateForces < ActiveRecord::Migration
  def change
    create_table :forces do |t|
      t.string :Person
      t.string :user
      t.string :mail

      t.timestamps null: false
    end
  end
end
