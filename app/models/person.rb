class Person < ActiveRecord::Base
    validates :nombre, presence: true
    has_many :organos
    before_save :count_organos
    def count_organos
        puts "a este usuario le quedan #{self.count_organos}"
    end
end
