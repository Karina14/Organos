json.array!(@forces) do |force|
  json.extract! force, :id, :Person, :user, :mail
  json.url force_url(force, format: :json)
end
